import urllib
import uuid
import requests

try:
    from config import config
except ImportError:
    class TestConfig(dict):
        def get_str(self, key, default):
            return self.get(key, default)
    config = TestConfig()


CONFIG_SERVER = "Magpie_server"
DEFAULT_SERVER = "http://magpie.hilibs.xyz"

WHITELIST = ["StartUp",
             "AsteroidCracked",
             "LaunchDrone",
             "MiningRefined",
             "SupercruiseExit",
             "StartJump",
             "Screenshot",
             "Location"]

BODY_MESSAGES = [
    "AsteroidCracked",
    "SupercruiseExit",
    "Screenshot",
    "Location"
]


class Session(object):
    """
    The game session
    """
    def __init__(self, logger, version=None, client=None):
        self.logger = logger
        self.system = None
        self.body = None
        self.id = str(uuid.uuid4())
        self.server = config.get_str(CONFIG_SERVER)
        if not self.server:
            self.server = DEFAULT_SERVER

        self.server = self.server.rstrip("/")
        self.version = version
        self.client = client
        # a list of events we should send when we know the body name we were at
        self.unsent_events = []

    def update_required(self):
        """
        Ask the server if we need an update
        :return:
        """
        r = requests.get("{}/api/v1/clientversion?client={}&version={}".format(self.server, self.client, self.version))
        if r and r.status_code == 200:
            resp = r.json()
            if resp:
                if resp.get("status") == "upgrade":
                    return resp.get("latest_download")
        return None

    def near_body(self):
        """
        Return True if we know the body we are near
        :return:
        """
        return self.system and self.body

    def get_body_url(self):
        """
        Return a url for the body name/link
        :return:
        """
        if self.near_body():
            try:
                system_safe = urllib.quote(self.system, safe="")
            except AttributeError:
                system_safe = urllib.parse.quote(self.system, safe="")

            return "{}/system/{}".format(self.server, system_safe)
        return ""

    def journal_entry(self, cmdr, system, station, entry, state):
        """
        Handle a journal event
        :param cmdr:
        :param system:
        :param station:
        :param entry:
        :param state:
        :return:
        """

        last_body = self.body

        event = entry["event"]
        if event not in WHITELIST:
            return

        self.system = system
        self.logger.write("Got {} event".format(event))

        if station:
            return  # ignore all events when docked

        if event == "StartJump":
            self.body = None

        # body messages
        if event in BODY_MESSAGES:
            self.body = entry.get("Body", None)

            if self.body != last_body:
                # if we now kwow the body we can send limpet and refined events for this body
                if self.body:
                    self.logger.write("Near body {}".format(self.body))
                    self.process_unsent(self.body)
                else:
                    if last_body:
                        self.logger.write("Left body {}".format(last_body))
                        self.process_unsent(last_body)

            # body location changed, wipe out any messages we couldn't identify the location for
            self.unsent_events = []

        # record our messages
        if event == "LaunchDrone":
            if entry.get("Type") in ["Prospector"]:
                self.add_event(entry, entry.get("Type"))
        if event == "AsteroidCracked":
            self.add_event(entry, event)
        if event == "MiningRefined":
            self.add_event(entry, entry.get("Type_Localised"))

    def send_event(self, msg, body=None):
        """
        Send this event to the magpie server
        :param msg:
        :return:
        """
        if body:
            msg["body"] = body
        url = self.server + "/api/v1/event"
        r = requests.post(url, json=msg)
        if not r.status_code == 200:
            self.logger.write("posting to url {} got {}".format(url, r.status_code))

    def add_event(self, entry, value):
        """
        Prepare a message to send (and send it if the body is known)
        :param entry:
        :param value:
        :return:
        """
        msg = {
            "time": entry["timestamp"],
            "session": self.id,
            "uuid": str(uuid.uuid4()),
            "starSystem": self.system,
            "body": self.body,
            "key": entry["event"],
            "value": value,
        }

        self.logger.write("{} {} at {}".format(msg["key"], msg["value"], msg["body"]))

        if self.near_body():
            self.send_event(msg)
        else:
            # save it for later
            self.unsent_events.append(msg)

    def process_unsent(self, body):
        """
        We just found out what the body was, send the unset messages with it set correctly
        :return:
        """
        if self.unsent_events and body:
            self.logger.write("Sending {} unset messages".format(len(self.unsent_events)))
            for event in self.unsent_events:
                self.send_event(event, body=body)
            self.unsent_events = []
