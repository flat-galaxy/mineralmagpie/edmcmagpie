"""
The plugin for MineralMagpie
"""
import os
import sys
import json

from logger import LogContext
from session import Session, CONFIG_SERVER, WHITELIST
from worker import Pool
try:
    import Tkinter as tk
except ImportError:
    import tkinter as tk

try:
    from config import config
    import myNotebook as nb
except ImportError:
    config = dict()
    nb = None
from ttkHyperlinkLabel import HyperlinkLabel


VERSION = "0.2.5"
PLUGNAME = "EDMCMagpiePlugin"
LOG = LogContext()
LOG.set_filename(os.path.join(os.path.dirname(__file__), "plugin.log"))
POOL = Pool(1, LOG)
SESSION = Session(LOG, version=VERSION, client=PLUGNAME)
PREF_SERVER = None

this = sys.modules[__name__]  # For holding module globals


def plugin_start():
    """
    Load the plugin
    :return:
    """
    global PREF_SERVER
    LOG.rotate_log()
    LOG.write("start MineralMagpie {} ({})".format(VERSION, __file__))
    LOG.write("ready!")
    PREF_SERVER = tk.StringVar(value=config.get_str(CONFIG_SERVER))


def plugin_start3(_):
    plugin_start()


def plugin_app(parent):
    """
    Create a pair of TK widgets for the EDMC main window
    """
    frame = tk.Frame(parent)

    this.bodyname_label = tk.Label(
        frame,
        text="Magpie!:",
        justify=tk.LEFT
    )
    this.bodyname_label.grid(row=1, column=0, sticky=tk.W)

    this.bodyname = HyperlinkLabel(
        frame, text="..loading", compound=tk.RIGHT, url="", name='ship')

    this.bodyname.grid(row=1, column=1, sticky=tk.EW)

    return frame


def plugin_prefs(parent, cmdr, isbeta):
    """
    Config tab
    :param parent:
    :return:
    """
    frame = nb.Frame(parent)
    frame.columnconfigure(1, weight=1)

    nb.Label(frame, text="Mineral Magpie Plugin Configuration").grid(padx=10, row=8, sticky=tk.W)

    nb.Label(frame, text="Server           ").grid(padx=10, row=10, sticky=tk.W)
    nb.Entry(frame, textvariable=PREF_SERVER).grid(padx=10, row=10, column=1, sticky=tk.EW)

    return frame


def prefs_changed(cmdr, isbeta):
    """
    Config value has changed
    :return:
    """
    changed = PREF_SERVER.get() != config.get(CONFIG_SERVER)
    if changed:
        LOG.write("configuration changed, reloading session")
        config.set(CONFIG_SERVER, PREF_SERVER.get())
        global SESSION
        SESSION = Session(LOG, version=VERSION, client=PLUGNAME)


def journal_entry(cmdr, beta, system, station, entry, state):
    """
    Process a journal event
    :param cmdr:
    :param beta:
    :param system:
    :param station:
    :param entry:
    :param state:
    :return:
    """
    if beta:
        return
    update = SESSION.update_required()
    if update:
        this.bodyname.configure(text="Plugin Update!", url=update)
        return

    POOL.begin(SESSION.journal_entry, cmdr, system, station, entry, state)
    if SESSION.near_body():
        this.bodyname.configure(text=SESSION.body, url=SESSION.get_body_url())
    else:
        this.bodyname.configure(text="...", url=None)


def do_import(server, folder):
    """
    Import journal files from folder
    :return:
    """
    s = Session(LOG, version=VERSION, client="offline-import")
    s.server = server

    system = None
    station = None

    files = os.listdir(folder)

    for filename in files:
        journal = os.path.join(folder, filename)
        if os.path.isfile(journal):
            with open(journal, "r") as journalfile:
                # load line by line
                while True:
                    line = journalfile.readline()
                    if not line:
                        break
                    try:
                        msg = json.loads(line)
                    except:
                        continue

                    if msg:
                        if "StarSystem" in msg:
                            system = msg.get("StarSystem")
                        if "Station" in msg:
                            station = msg.get("Station")

                        if msg.get("event") in ["MarketSell"]:
                            station = "set"

                        if msg.get("event") in ["LaunchDrone", "SupercruiseExit", "AsteroidCracked"]:
                            station = None

                        if system:
                            if msg.get("event") in WHITELIST:
                                print(" {} {} {}".format(system, s.body, msg.get("event")))
                                s.journal_entry(None, system, station, msg, None)


if __name__ == "__main__":
    do_import(sys.argv[1], sys.argv[2])
